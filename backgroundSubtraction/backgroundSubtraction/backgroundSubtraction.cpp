// backgroundSubtraction.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <fstream>      // std::ifstream
#include <string>
#include <Windows.h>

#define PI (float)3.1415926535897

using namespace cv;
using namespace std;

int SigmaDelta(string fname,int rows,int cols) {

	ifstream ifiles;
	string line;
	Mat image;
	Mat bgr[3];
	Mat b, b0, b1;
	int N = 64;

	Mat O(rows/6, cols/6, CV_16S, Scalar(0));
	Mat V(rows/6, cols/6, CV_16S, Scalar(0));
	Mat M(rows/6, cols/6, CV_16S, Scalar(0));
	Mat E(rows/6, cols/6, CV_16U,  Scalar(0));
	stringstream convert;

	ifiles.open(fname, ifstream::in);

	if (ifiles.is_open())
	{
		while (ifiles.good())
		{
			getline(ifiles, line);
			image = imread(line, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);

			FILETIME ft;
			LARGE_INTEGER li;


			GetSystemTimeAsFileTime(&ft);
			li.LowPart = ft.dwLowDateTime;
			li.HighPart = ft.dwHighDateTime;

			uint64 t0 = li.QuadPart;
			t0 -= 116444736000000000LL; /* Convert from file time to UNIX epoch time. */
			t0 /= 10000; /* From 100 nano seconds (10^-7) to 1 millisecond (10^-3) intervals */

			Mat dst(rows / 6, cols / 6, CV_16UC3, Scalar(0));
			resize(image, dst, dst.size(), 0, 0, INTER_AREA);
			split(dst, bgr);

			b = bgr[2] + bgr[1]+ bgr[0];
			b = b * 16;
			
			//resize(b, dst, dst.size(), 0, 0, INTER_AREA);
			for (int i = 0; i < dst.rows; i++) {
				for (int j = 0; j < dst.cols; j++) {

					if (M.at<__int16>(i, j) < (__int16)b.at<unsigned short>(i, j)) {

						if (M.at<__int16>(i, j) > 30000) {
							M.at<__int16>(i, j) = 30000;
						}
						else {
							M.at<__int16>(i, j) += 512;
						}
					}
					else if (M.at<__int16>(i, j) > (__int16)b.at<unsigned short>(i, j)) {
						if (M.at<__int16>(i, j) < -30000) {
							M.at<__int16>(i, j) = -30000;
						}
						else {
							M.at<__int16>(i, j) -= 512;
						}
					}
					
					O.at<__int16>(i, j) = abs(M.at<__int16>(i, j) - (__int16)b.at<unsigned short>(i, j));

					__int16 temp0 = O.at<__int16>(i, j);

					if (V.at<__int16>(i, j) < N*O.at<__int16>(i, j)) {

						if (V.at<__int16>(i, j) > 30000) {
							V.at<__int16>(i, j) = 30000;
						}
						else {
							V.at<__int16>(i, j) += 512;
						}
					}
					else if (V.at<__int16>(i, j) > N*O.at<__int16>(i, j)) {

						if (V.at<__int16>(i, j) < -30000) {
							V.at<__int16>(i, j) = -30000;
						}
						else {
							V.at<__int16>(i, j) -= 512;
						}
					}

					__int16 temp1 = V.at<__int16>(i, j);

					if (V.at<__int16>(i, j) > O.at<__int16>(i, j)) {
						E.at<unsigned short>(i, j) = 0;
					}
					else {
						E.at<unsigned short>(i, j) = 65535;
					}



				}
			}
			GetSystemTimeAsFileTime(&ft);
			li.LowPart = ft.dwLowDateTime;
			li.HighPart = ft.dwHighDateTime;

			uint64 t1 = li.QuadPart;
			t1 -= 116444736000000000LL; /* Convert from file time to UNIX epoch time. */
			t1 /= 10000; /* From 100 nano seconds (10^-7) to 1 millisecond (10^-3) intervals */

			uint64 t = t1 - t0;

			convert << line << "  " << t;
			string hd = convert.str();
			convert.clear();//clear any bits set
			convert.str(string());
 

			namedWindow("motion", CV_WINDOW_NORMAL);// Create a window for display.
			resizeWindow("motion", (int)(cols/4), (int)(rows /4));
			putText(E, hd, CvPoint(10, 190), FONT_HERSHEY_SIMPLEX, .3,
				Scalar::all(65535), 1, 8);
			imshow("motion", E);                   // Show our image inside it.

			waitKey(1);
			

		}
	}
	else {
		return 1;
	}

	return 0;
}

int main(int argc, char * argv[])
{
	Mat image;
	int m, n = 0;
	double alpha =16.2;
	double beta = 5000;
	string filename;


	string line;
	ifstream ifiles;

	if (argc < 2) {
		cout << "enter a file name";

	}

	 

	ifiles.open(string(argv[1]), ifstream::in);

	if (ifiles.is_open())
	{
	
		getline(ifiles, line);
		image = imread(line, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);

		n = image.rows;
		m = image.cols;
		Mat new_image( n,m, CV_16UC3);
		for (int y = 0; y < image.rows; y++)
		{
			for (int x = 0; x < image.cols; x++)
			{
				for (int c = 0; c < 3; c++)
				{
					new_image.at<Vec3w>(y, x)[c] =
						saturate_cast<ushort>(alpha*(image.at<Vec3w>(y, x)[c]) + beta);
				}
			}
		}

		namedWindow("Display window", CV_WINDOW_NORMAL);// Create a window for display.
		resizeWindow("Display window", (int)(m / 1.3), (int)(n / 1.3));
		imshow("Display window", new_image);

		waitKey(0);
		destroyWindow("Display window");

	}
	ifiles.close();

	SigmaDelta(string(argv[1]),  n,m);


    return 0;
}

