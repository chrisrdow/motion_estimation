#pragma once
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/imgproc/imgproc.hpp>

///////////////////////////////////////////////////////////////////////////////////////////////////
class Blob {
public:
	// member variables ///////////////////////////////////////////////////////////////////////////
	//std::vector<cv::Point> currentContour;

	cv::Rect currentBoundingRect;

	std::vector<cv::Point> centerPositions;
	float dx;
	float dy;
	int frame;
	int id;
	double dblCurrentDiagonalSize;
	//double dblCurrentAspectRatio;

	bool blnCurrentMatchFoundOrNewBlob;

	bool blnStillBeingTracked;
	bool blnStopped;

	int intNumOfConsecutiveFramesWithoutAMatch;

	cv::Point predictedNextPosition;
	cv::Mat im_r;

	// function prototypes ////////////////////////////////////////////////////////////////////////
	Blob(cv::Point2f _center);
	void predictNextPosition(void);
	cv::Point2f predict();
	void update(cv::Point2f newPt) {
		if ((abs(centerPositions[0].x - (int)newPt.x) < 7) && (abs(centerPositions[0].y - (int)newPt.y) < 7)) {
			blnStopped = true;
		}
		else {
			blnStopped = false;
		}

		centerPositions[0].x = (int)newPt.x;
		centerPositions[0].y = (int)newPt.y;

	//	dx = newPt.x - centerPositions[0].x ;
	//	dy = newPt.y - centerPositions[0].y;

	}

};
